<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lookup".
 *
 * @property int $id
 * @property string $name
 * @property int $code
 * @property string $type
 * @property int $position
 */
class Lookup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lookup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'type', 'position'], 'required'],
            [['code', 'position'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'type' => 'Type',
            'position' => 'Position',
        ];
    }

    private static $_items = [];
     
    public static function items($type)
    {
		$status_list = [];
		$count = 1;
        $models = self::find()->where(['type' => $type])->orderBy('position')->all();
		foreach ($models as $model) {
			$status_list[$count] = $model->name;
			$count++;
        }
        return $status_list;
    }

    public static function item($type, $code)
    {
        $model = self::find()->where(['type' => $type, 'code' => $code])->one();
        return $model->name;
    }
}

