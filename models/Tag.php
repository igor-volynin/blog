<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $name
 * @property int $frequency
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['frequency'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'frequency' => 'Frequency',
        ];
    }

    public static function string2array($tags)
    {
            return preg_split('/\s*,\s*/',trim($tags),-1,PREG_SPLIT_NO_EMPTY);
    }

    public static function array2string($tags)
    {
            return implode(', ',$tags);
    }
    
    public function updateFrequency($oldTags, $newTags)
    {
        $oldTags = self::string2array($oldTags);
        $newTags = self::string2array($newTags);
        self::addTags(array_values(array_diff($newTags, $oldTags)));
        self::removeTags(array_values(array_diff($oldTags, $newTags)));
    }

    public function addTags($tags)
    {
        foreach ($tags as $name) {
            if ($tag = self::find()->where(['name' => $name])->one()) {
                $tag->updateCounters(['frequency' => 1]);
            } else {
                $tag = new Tag;
                $tag->name = $name;
                $tag->frequency = 1;
                $tag->save();
            }
        }
    }

    public function removeTags($tags)
    {
        if (empty($tags)) {
            return;
        }

        foreach ($tags as $name) {
            $tag = self::find()->where(['name' => $name])->one();
            $tag->updateCounters(['frequency' => -1]);
            if ($tag->frequency <= 0) {
                $tag->delete();
            }
        }
    }

    public function findTagWeights($limit=20)
    {
        $models = self::find()
            ->orderBy(['frequency' => SORT_DESC])
            ->limit($limit)
            ->all();

        $total = 0;
        foreach ($models as $model) {
            $total+=$model->frequency;
        }

        $tags = [];

        if ($total>0) {
            foreach ($models as $model) {
                $tags[$model->name]=8+(int)(16*$model->frequency/($total+10));
            }
            ksort($tags);
        }
        return $tags;
    }
}
