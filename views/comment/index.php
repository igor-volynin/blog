<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

<div class="container">

    <div class="blog-header">
        <h1 class="blog-title">Неодобренные комментарии</h1>
    </div>
    <br>
    <div class="col-sm-8 blog-main">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'content:ntext',
            'status',
            'create_time:datetime',
            'author',

            ['class' => 'yii\grid\ActionColumn',
             'template' => '{view} {update} {approve} {delete}',
             'buttons' => [
                'approve' => function ($url, $model, $key) {
                     return Html::a('<span class="glyphicon glyphicon-check"></span>', 'approve?id='.$model->id,
                        ['title' => 'Approve']);
                }
                ],
            ],
        ]
    ]); ?>
    </div>

    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    	<?= $this->render('/post/sidebar.php'); ?>
    </div>

</div>
</div>
