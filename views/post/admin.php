<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin';
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

<div class="container">

    <div class="blog-header">
        <h1 class="blog-title">Управление записями</h1>
    </div>
    
    <br>

    <div class="col-sm-8 blog-main">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'content:ntext',
            'tags:ntext',
            'status',
            //'create_time:datetime',
            //'update_time:datetime',
            //'author_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
    
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    	<?= $this->render('sidebar.php'); ?>
    </div>

</div>
</div>
