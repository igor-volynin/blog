<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use app\models\Tag;
use app\models\Comment;
use app\models\Post;

?>
<div class="sidebar-module">
    
    <?php if (!Yii::$app->user->isGuest): ?>
    <div class="panel panel-default">
    <?= Nav::widget([
        'options' => ['class' => 'nav'],
        'items' => [
            ['label' => 'Одобрить комментарий (' . count(Comment::find()->where(['status' => Comment::STATUS_PENDING])->all()) . ')', 'url' => ['/comment']],
            ['label' => 'Создать новую запись', 'url' => ['/post/create']],
            ['label' => 'Управление записями', 'url' => ['/post/admin']],
        ],
    ]); ?>
    </div>
    <?php endif; ?>
     
    <br>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Теги</h3>
        </div>
        <div class="panel-body">
            <?php $tags=Tag::findTagWeights(20); ?>    
            <?php foreach ($tags as $tag=>$weight): ?>
            <?php 
                $link = Html::a(Html::encode($tag), ['post/index', 'tag' => $tag]); 
                echo Html::tag('span', $link, ['class' => 'tag', 'style' => 'font-size:'.$weight.'pt', 'margin' => 'auto']);
            ?>
            <?php endforeach;?>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Комментарии</h3>
        </div>
    <?php $comments = Comment::findRecentComments(); ?>
    <ul class="list-unstyled">
    <?php foreach ($comments as $comment): ?>
        <p><li style="margin-left: 16px"><?= $comment->author . ' on ' ?>
        <?= Html::a($comment->post->title, ['/post/view', 'id' => $comment->post->id, '#' => 'c'.$comment->id]); ?></li></p>
    <?php endforeach; ?>
    </ul>
    </div>

</div>

