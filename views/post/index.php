<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blog';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

    <div class="col-sm-8 blog-main">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
	'summary' => false,
    ]); ?>
    </div>

    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    	<?= $this->render('sidebar.php'); ?>
    </div>
</div>
</div>
