<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

    <div class="blog-header">
        <h1 class="blog-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-sm-8 blog-main">
        <?= $this->render('_view', ['model' => $model]) ?>
    </div>
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    	<?= $this->render('sidebar.php'); ?>
    </div>
</div>
<div class="col-sm-8 blog-main">

        <?php if (Yii::$app->session->hasFlash('commentSubmitted')): ?>
        <div class="alert alert-success alert dismissble" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('commentSubmitted'); ?>
        </div>
        <?php endif; ?>
    
        <?php if ($model->commentsCount >= 1): ?>
    
        <h3>
            <?php echo $model->commentsCount . ' comment(s)'; ?>
        </h3>
   
        <?= $this->render('_comments', [
            'model' => $model,
            'comments' => $model->comments
        ]) ?>
        <?php endif; ?>
            
        <h3>Оставить комментарий</h3>

        <?= $this->render('/comment/_form', ['model' => $comment]); ?>
    
</div>
        
