<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = 'Create Post';
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-create">

<div class="container">

    <div class="blog-header">
        <h1 class="blog-title">Создать новую запись</h1>
    </div>
    
    <br>

    <div class="col-sm-8 blog-main">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
   
    <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
    	<?= $this->render('sidebar.php'); ?>
    </div>

</div>
</div>
