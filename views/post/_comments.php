<?php

use yii\helpers\Html;

?>
<?php foreach($comments as $comment): ?>
<div class="comment" id="c<?php echo $comment->id; ?>">

    <?= Html::a('#' . $comment->id, ['view?id='.$model->id.'#c'.$comment->id]); ?>

    <div class="author">
        <?php echo $comment->author; ?> says:
    </div>

    <div class="time">
        <?php echo date('F j, Y \a\t h:i a',$comment->create_time); ?>
    </div>

    <div class="content">
        <?= Html::encode($comment->content) ?>
    </div>
    <br>
</div><!-- comment -->
<?php endforeach; ?>
