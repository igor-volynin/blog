<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="row">

    <div class="blog-post">
        <h2 class="blog-post-title"><a href="<?= Url::to(['post/view', 'id' => $model->id]) ?>"><?= $model->title ?></a></h2>
    </div>    
    <p class="blog-post-meta"><?= Html::encode('posted by ' . $model->author->username . ' on ' . date('F j, Y',$model->create_time)) ?></p>
    <p><?= Html::encode($model->content) ?></p>	
    <?= "Tags: ".implode(', ', $model->tagLinks); ?>

</div>
