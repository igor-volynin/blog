<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lookup`.
 */
class m180926_145115_create_lookup_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lookup', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'code' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'position' => $this->integer()->notNull()
        ]);

        $this->batchInsert('lookup', ['name', 'type', 'code', 'position'], [
            ['Draft', 'PostStatus', 1, 1],
            ['Published', 'PostStatus', 2, 2],
            ['Archived', 'PostStatus', 3, 3],
            ['Pending Approval', 'CommentStatus', 1, 1],
            ['Approved', 'CommentStatus', 2, 2]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lookup');
    }
}
