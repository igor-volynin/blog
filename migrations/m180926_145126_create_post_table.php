<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180926_145126_create_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'tags' => $this->text(),
            'status' => $this->integer()->notNull(),
            'create_time' => $this->integer(),
            'update_time' => $this->integer(),
            'author_id' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-author_id',
            'post',
            'author_id');

        $this->addForeignKey(
            'fk-author_id',
            'post',
            'author_id',
            'user',
            'id',
            'CASCADE');

        $this->insert('post', [
            'title' => 'Welcome',
            'content' => 'This blog system is developed using Yii. It is meant to demonstrate how to use Yii to build a complete real-world application. Complete source code may be found in the Yii releases. Feel free to try this system by writing new posts and posting comments.',
            'status' => 2,
            'create_time' => 1230952187,
            'update_time' => 1230952187,
            'author_id' => 1,
            'tags' => 'yii, blog'
        ]);
        
        $this->insert('post', [
            'title' => 'A Test Post',
            'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 
            'status' => 2,
            'create_time' => 1230952187,
            'update_time' => 1230952187,
            'author_id' => 1,
            'tags' => 'test'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
