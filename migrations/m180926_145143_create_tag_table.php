<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tag`.
 */
class m180926_145143_create_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tag', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'frequency' => $this->integer()->defaultValue(1)
        ]);

        $this->insert('tag', ['name' => 'yii']);
        $this->insert('tag', ['name' => 'blog']);
        $this->insert('tag', ['name' => 'test']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tag');
    }
}
