<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m180926_145138_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'content' => $this->text()->notNull(),
            'status' => $this->integer()->notNull(),
            'create_time' => $this->integer(),
            'author' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'url' => $this->string(),
            'post_id' => $this->integer()->notNull()
        ]);

        $this->createIndex(
            'idx-post_id',
            'comment',
            'post_id');

        $this->addForeignKey(
            'fk-post_id',
            'comment',
            'post_id',
            'post',
            'id',
            'CASCADE');

        $this->insert('comment', [
            'content' => 'This is a test comment',
            'status' => 2,
            'create_time' => 1230952187,
            'author' => 'Tester',
            'email' => 'tester@example.com',
            'post_id' => 2
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }
}
